import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";
import { Pokemon } from "../../models/pokemon.model";

export const pokemonServiceApi = createApi({
    reducerPath: 'pokemonServiceApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:4000/' }),
    tagTypes: ['Pokemon'],
    endpoints: (builder) => ({
        getPokemons: builder.query<Pokemon[],void>({
            query: () => '/pokemons',
            providesTags: ['Pokemon']
        }),
        getPokemonById: builder.query<Pokemon, string>({
            query: id => `/pokemons/${id}`,
            providesTags: ['Pokemon']
        }),
        getPokemonByName: builder.query<Pokemon, string>({
            query: name => `/pokemons?name=${name}`,
            providesTags: ['Pokemon']
        }),
        addPokemon: builder.mutation<void, Pokemon>({
            query: pokemon => ({
                url: '/pokemons',
                method: 'POST',
                body: pokemon
            }),
            invalidatesTags: ['Pokemon']
        }),
        updatePokemon: builder.mutation<void, Pokemon>({
            query: ({id, ...rest}) => ({
                url: `/pokemons/${id}`,
                method: 'PUT',
                body: rest
            }),
            invalidatesTags: ['Pokemon']
        }),
        deletePokemon: builder.mutation<void, string>({
            query: (id) => ({
                url: `/pokemons/${id}`,
                method: 'DELETE'
            }),
            invalidatesTags: ['Pokemon']
        })
    })
})

export const { 
    useGetPokemonsQuery, 
    useGetPokemonByIdQuery, 
    useGetPokemonByNameQuery,
    useAddPokemonMutation,
    useUpdatePokemonMutation,
    useDeletePokemonMutation
} = pokemonServiceApi;            