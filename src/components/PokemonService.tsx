import { useState } from "react";
import { Pokemon } from "../models/pokemon.model";
import { 
    useGetPokemonByIdQuery, 
    useGetPokemonsQuery, 
    useGetPokemonByNameQuery, 
    useAddPokemonMutation, 
    useUpdatePokemonMutation, 
    useDeletePokemonMutation 
} from "../redux/services/pokemonService"
import Card from "./Card";

const PokemonService = () => {
    const { data, error, isLoading, isFetching, isSuccess } = useGetPokemonsQuery();
    const [namePokemon, setNamePokemon] = useState('bulbasour')

    

    return (
        <div>
            <h1>React Redux Toolkit RTK Query Tutorial</h1>
            <input type="text" value={namePokemon} />
            {isLoading && <h2>...Loading</h2>}
            {isFetching && <h2>...Fetching</h2>}
            {error && <h2>Something went wrong</h2>}
            {isSuccess && (
                <div className="row">
                    {data?.map((pokemon) => {
                        return (
                            
                                <Card name={pokemon.name} img={pokemon.img}>

                                </Card>

                        )
                    })}
                </div>
            )}
            <div><AddPokemon></AddPokemon></div>

        </div>
    )
}

export default PokemonService

export const PokemonDetailById = ({ id }: { id: string }) => {
    const { data } = useGetPokemonByIdQuery(id);

    return (
        <>
            <h3>Busqueda por ID:</h3>
            <pre>{JSON.stringify(data, undefined, 2)}</pre>
        </>
    )
}

export const PokemonDetailByName = ({ name }: { name: string }) => {
    const { data } = useGetPokemonByNameQuery(name);

    return (
        <>
            <h3>Busqueda por Name</h3>
            <pre>{JSON.stringify(data, undefined, 2)}</pre>
        </>
    )
}

export const AddPokemon = () => {
    const [addContact] = useAddPokemonMutation()
    const [updatePokemon] = useUpdatePokemonMutation()
    const [deletePokemon] = useDeletePokemonMutation()

    const pokemon: Pokemon = {
        id: 10,
        name: "charizard",
        img: "https://raw.githubusercontent.com/HybridShivam/Pokemon/master/assets/images/006-Gigantamax.png"
    }

    const pokemonUpdate: Pokemon = {
        id: 10,
        name: "charizard Update",
        img: "https://raw.githubusercontent.com/HybridShivam/Pokemon/master/assets/images/006-Gigantamax.png"
    }

    const addHandler = async () => {
        await addContact(pokemon)
    }

    const updateHandler = async () => {
        await updatePokemon(pokemonUpdate)
    }
    const deleteHandler = async () => {
        await deletePokemon(pokemon.id)
    }

    return (
        <>  
            
            <button onClick={addHandler}>Add Pokemon</button>
            <button onClick={updateHandler}>Update Pokemon</button>
            <button onClick={deleteHandler}>DeletePokemon</button>

        </>
    )
}