import { useGetPokemonByNameQuery } from "../redux/services/pokemonService"
import Card from "./Card"

interface Pokemon {
    name: string
}

const Pokemon = (pokemon: Pokemon) => {
    const { data, error, isLoading, isFetching, isSuccess } = useGetPokemonByNameQuery()

    return (
        <div>
            {
                error ?
                    <>Oh no, there was an error</>
                : isLoading?
                    (<>Loading....</>)
                : data ?
                    (
                        <Card name={data.name} img={data.img}></Card>
                    )
                : null
            }
        </div>
    )

}

export default Pokemon