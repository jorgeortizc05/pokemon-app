import styles from '../../styles/Home.module.css'

interface Card {
    name: string,
    img: string
}

const Card = (card: Card) => {
    return (
        <div className={`card ${styles.cardpokemon}`}>
            <img src={card.img} className="card-img-top" alt="..."/>
                <div className="card-body">
                    <h5 className="card-title">{card.name}</h5>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
        </div>
    )
}

export default Card