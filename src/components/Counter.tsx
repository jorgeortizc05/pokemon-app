import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../redux/app/hooks";
import { increment, decrement, incrementByAmount } from "../redux/features/counter/counterSlice";


const Counter = () => {
    const count = useAppSelector((state) => state.counter.value)
    const dispatch = useAppDispatch()
    const [step, setStep] = useState(2)

    return (
        <div>
            <div>
                <input type="text" value={step} onChange={(e) => setStep(e.target.value)}></input>
                <button
                    aria-label="Increment value"
                    onClick={() => dispatch(increment())}
                >
                    Increment
                </button>
                <span>{count}</span>
                <button
                    aria-label="Decrement value"
                    onClick={() => dispatch(decrement())}
                >
                    Decrement
                </button>
                <button
                    aria-label="Decrement value"
                    onClick={() => dispatch(incrementByAmount(Number(step)))}
                >
                    Decrement
                </button>
            </div>
        </div>
    )
}

export default Counter;