import type { NextPage } from 'next'
import Pokemon from '../components/Pokemon'

const namesPokemons = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

const Home: NextPage = () => {
  return (
    <div className="container">
      <div className='row'>
        {namesPokemons.map((namePokemon) => { 
            return (<Pokemon name={namePokemon}></Pokemon>) 
          })
        }
      </div>
    </div>
  )
}

export default Home
